(local {: hitbox} (include :structs))

(import-macros {: get! : set!} :fennel-struct)

(local hb (hitbox 10 10 30 30))

(let [(x y w h) (get! hb hitbox :x :y :w :h)]
  (print x y w h))

(set! hb hitbox :x 11 :y 12)

(let [(x y w h) (get! hb hitbox :x :y :w :h)]
  (print x y w h))
