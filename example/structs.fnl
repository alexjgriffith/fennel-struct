(import-macros { : def-struct} :fennel-struct)

(def-struct hitbox
  :x :number
  :y :number
  :h :number
  :w :number)

{: hitbox}
